<?php

namespace Drupal\twitter_api_client;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\RandomGeneratorTrait;

/**
 * Tests the TwitterAPI Client install defaults.
 *
 * @group twitter_api_client
 */
class TwitterAPIClientDefaultsTest extends KernelTestBase {

  use RandomGeneratorTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['system', 'node', 'user', 'twitter_api_client'];

  /**
   * The storage manager.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $defaultStorage;

  /**
   * Setup common code for test cases.
   */
  protected function setup() : void {
    parent::setUp();

    $this->installSchema('system', 'sequences');
    $this->installConfig('system');

    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installConfig('user');
    $this->installConfig('twitter_api_client');
    $this->defaultStorage = \Drupal::entityTypeManager()
      ->getStorage('book_access_defaults');
  }

  /**
   * Test that default config is created on install.   *.
   */
  public function testInstallCreatesDefaultConfig() {
    $settings = $this->config('twitter_api_client.settings');
    $string = $settings->get('oauth_bearer_key');
    $this->assertEquals($string, "", "Empty default result expected.");

    $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo('block_content');
    $this->assertArrayHasKey("twitter_feed", $bundles ,
      "Expected to find a block content bundle \"twitter_feed\"");
  }

}
