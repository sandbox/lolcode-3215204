## CONTENTS OF THIS FILE ##

* Introduction
* Configuration
* Usage

### Introduction

This module provides a custom block type "twitter_feed" that contains a field to enter search parameters.
The block then displays the feed using the custom "feed" field.

### Configuration
A valid Twitter API Oath Bearer token must be added to local.settings.php.
The default template for this block can of course be overridden.
The custom block can have additional fields added as needed, operation will not be affected.

### Usage

Create a custom block entity and enter:
* A feed ID: This is an identifier to allow several feeds to be saved and retrieved from Drupal State storage.
* A feed search: This is a valid search expression according to the Twitter API v2.
