// parses the text in a tweet and adds links to users, hash tags and urls
function tacParseTweet (text) {
  // parse urls
  text = text.replace(/[A-Za-z]+:\/\/[A-Za-z0-9_-]+\.[A-Za-z0-9_:%&~\?\/.=-]+/g, function(url) {
    return url.link(url);
  })
  // parse usernames
  text = text.replace(/[@]+[A-Za-z0-9_-]+/g, function(u) {
    var username = u.replace("@","");
    return u.link("https://twitter.com/" + username);
  })
  // parse hashtags
  text = text.replace(/[#]+[A-Za-z0-9_-]+/g, function(t) {
    var tag = t.replace("#","%23");
    return t.link("https://twitter.com/hashtag/" + tag + "?src=hashtag_click");
  })
  return text;
}

(function ($, Drupal) {
  /**
   * Initialize all Foundation JavaScript plugins.
   */
  Drupal.behaviors.tacFormatTweet = {
    attach: function (context, settings) {
      // Using once() with more complexity.
      $('.twitter-api-client__results .twitter-api-client__tweet__text', context)
      .once('tacProcessText').each(function () {
        $(this).html(tacParseTweet($(this).text()));
      });
    }
  };

})(jQuery, Drupal);
