<?php


namespace Drupal\twitter_api_client\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Twitter API Settings.
 */
class TwitterAPIClientSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'twitter_api_client_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['twitter_api_client.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $twitter_config = $this->configFactory->get('twitter_api_client.settings');

    $form['oauth_bearer_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OAuth Bearer Key'),
      '#default_value' => $twitter_config->get('oauth_bearer_key'),
      '#required' => TRUE,
    ];
    $form['api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Url'),
      '#default_value' => $twitter_config->get('api_url'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $twitter_config = $this->configFactory->getEditable('twitter_api_client.settings');
    $twitter_config
      ->set('oauth_bearer_key', $form_state->getValue('oauth_bearer_key'))
      ->set('api_url', $form_state->getValue('api_url'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}

