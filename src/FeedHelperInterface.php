<?php

namespace Drupal\twitter_api_client;

interface FeedHelperInterface {

  function doRecentTweetSearch(array $feed_info) : string;

  function getFeedsList() : array;

  function themeFeed(array $feed);

}
