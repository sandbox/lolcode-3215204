<?php

namespace Drupal\twitter_api_client;

use DateTime;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use lolaslade\twitterapiclient\TwitterApiClient;
use Psr\Log\LoggerInterface;

class FeedHelper implements FeedHelperInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config_factory;

  /**
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * @var \GuzzleHttp\ClientInterface
   */
  private $client;

  /**
   * @var \lolaslade\twitterapiclient\TwitterApiClient
   */
  private $twitterApiClient;

  /**
   * Constructs a new FeedFetcher.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager,
                              ConfigFactoryInterface $config_factory,
                              StateInterface $state,
                              ClientInterface $client,
                              LoggerInterface $logger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->config_factory = $config_factory;
    $this->state = $state;
    $this->client = $client;
    $this->logger = $logger;
    $this->twitterApiClient = new TwitterApiClient($this->client,
      strval($this->config_factory->get('twitter_api_client.settings')->get('oauth_bearer_key')));
  }

  /**
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  function getFeedsList(): array {
    $list = [];
    $query = $this->entityTypeManager->getStorage('block_content')
      ->getQuery();
    $block_ids = $query->condition('type', 'twitter_feed')
      ->execute();
    $blocks = $this->entityTypeManager->getStorage('block_content')
      ->loadMultiple($block_ids);
    /** @var \Drupal\block_content\BlockContentInterface $block */
    foreach ($blocks as $block) {
      if ($block->hasField('feed_id') && $block->hasField('query')) {

        $feed_id = strval($block->get('feed_id')->getValue()[0]['value']);
        $query = strval($block->get('query')->getValue()[0]['value']);
        $max_results = 25;
        $fields = null;
        $expansions = null;
        if ($block->hasField('max_results')) {
          $max_results = intval($block->get('max_results')->getValue()[0]['value']);
        }
        $refresh_interval = 300; // 5 minutes
        if ($block->hasField('refresh_interval')) {
          $refresh_interval = intval($block->get('refresh_interval')->getValue()[0]['value']);
        }
        if ($block->hasField('api_fields')) {
          $fields = strval($block->get('api_fields')->getValue()[0]['value']);
        }
        if ($block->hasField('api_expansions')) {
          $expansions = strval($block->get('api_expansions')->getValue()[0]['value']);
        }
        $list[$feed_id] = [
          'feed_id' => $feed_id,
          'query' => $query,
          'fields' => $fields,
          'expansions' => $expansions,
          'max_results' => $max_results,
          'refresh_interval' => $refresh_interval,
        ];

      }
    }
    return $list;
  }

  function doRecentTweetSearch(array $feed_info): string {
    $result = null;
    try {
      $this->twitterApiClient->setGetField('media.fields', 'media_key,type,height,width,preview_image_url,url');
      $this->twitterApiClient->setGetField('user.fields', 'id,name,username,created_at,description,profile_image_url');
      $result = $this->twitterApiClient->doRecentTweetSearch($feed_info['query'],
        $feed_info['fields'], $feed_info['expansions'], $feed_info['max_results']);
    } catch (GuzzleException $e) {
      $message = \Drupal::translation()->translate('Unable to retrieve [%feed_id]: %message',
        [
          '%feed_id' => $feed_info['feed_id'],
          '%message' => $e->getMessage(),
        ]);
      $this->logger->error($message);
    }
    return $result;
  }

  /**
   * @param array $feed
   *
   * @return array
   */
  function themeFeed(array $feed) : array {
    $found_feed = isset($feed);
    $items = [];
    $api_result = json_decode($feed['data'], TRUE);
    $meta = $api_result['meta'] ?? [];

    if ($found_feed) {
      $items = $this->themeFeedItems($api_result, $items);
    }

    return [
      '#theme' => 'recent_tweet_search',
      '#items' => $items,
      '#feed_found' => $found_feed,
      '#last_updated' => $found_feed ? ($feed['last_updated'] ?? -1) : -1,
      '#meta' => $meta,
      '#refresh_interval' => $found_feed ? ($feed['refresh_interval'] ?? -1) : -1,
    ];
  }

  /**
   * @param $api_result
   * @param array $items
   *
   * @return array
   */
  protected function themeFeedItems($api_result, array $items): array {
    $users = [];
    $attachments = [];
    $referenced_tweets = [];

    /** @var \Drupal\Core\Datetime\DateFormatterInterface $formatter */
    $date_formatter = \Drupal::service('date.formatter');
    $request_time = \Drupal::time()->getRequestTime();

    // Add keys to users
    if (isset($api_result['includes']['users'])) {
      foreach ($api_result['includes']['users'] as $user) {
        $users[$user['id']] = $user;
      }
    }
    // Add keys to attachments
    if (isset($api_result['includes']['media'])) {
      foreach ($api_result['includes']['media'] as $media) {
        $attachments[$media['media_key']] = $media;
      }
    }
    // Add keys to tweets
    if (isset($api_result['includes']['tweets'])) {
      foreach ($api_result['includes']['tweets'] as $tweet) {
        $referenced_tweets[$tweet['id']] = $tweet;
      }
    }

    foreach ($api_result['data'] as $datum) {
      $retweetObject = NULL;
      if (isset($datum['created_at'])) {
        $created_date = new DateTime($datum['created_at']);
        $created_at = $date_formatter->formatDiff($created_date->getTimestamp(), $request_time, [
          'granularity' => 1,
          'return_as_object' => TRUE,
        ])->toRenderable();
      }
      $references = $datum['referenced_tweets'] ?? [];
      foreach ($references as $reference) {
        if ($reference['type'] == 'retweeted') {
          $retweetObject = $referenced_tweets[$reference['id']];
        }
      }
      if (empty($retweetObject)) {
        $author_id = $datum['author_id'];
        $retweet_author_id = '';
        $primary_image = ($datum['attachments']['media_keys'][0] ?? false) ?
          ($attachments[$datum['attachments']['media_keys'][0]] ?? NULL) : null;
      }
      else {
        $author_id = $retweetObject['author_id'];
        $retweet_author_id = $datum['author_id'];
        $primary_image = ($retweetObject['attachments']['media_keys'][0] ?? false) ?
          ($attachments[$retweetObject['attachments']['media_keys'][0]] ?? null) : null;
      }
      $authorObject = $users[$author_id] ?? [];
      $retweetAuthorObject = $users[$retweet_author_id] ?? [];
      $text = $datum['text'] ?? '';

      $items[] = [
        '#theme' => 'tweet',
        '#author_id' => $author_id,
        '#author_object' => $authorObject,
        '#created_at' => $created_at ?? '',
        '#id' => $datum['id'],
        '#primary_image' => $primary_image,
        '#retweet_author_id' => $retweetObject,
        '#retweet_author_object' => $retweetAuthorObject,
        '#retweet_object' => $retweetObject,
        '#text' => $text,
        '#media' => $attachments,
        '#users' => $users,
      ];
    }
    return $items;
  }

}
